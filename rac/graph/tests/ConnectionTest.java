/*
 * Copyright 2014 Julian Andres Klode <klode@mathematik.uni-marburg.de>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package rac.graph.tests;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import static org.junit.Assert.*;
import org.junit.Test;

import rac.graph.*;

public class ConnectionTest {

    @Test
    public void testAlpha() throws FileNotFoundException {
        Graph graph = new Graph(new Scanner(new File("examples/alpha.txt")));

        assertEquals(1, graph.calculateScoreOf(Affiliation.ROME));
        assertEquals(1, graph.calculateScoreOf(Affiliation.CARTHAGE));
    }

    @Test
    public void testDelta() throws FileNotFoundException {
        Graph graph = new Graph(new Scanner(new File("examples/delta.txt")));

        assertEquals(3, graph.getConnectedComponent(graph.getById(1)).size());
        assertEquals(3, graph.getConnectedComponent(graph.getById(2)).size());
        assertEquals(3, graph.getConnectedComponent(graph.getById(3)).size());
        assertEquals(3, graph.getConnectedComponent(graph.getById(4)).size());
        assertEquals(3, graph.getConnectedComponent(graph.getById(5)).size());
        assertEquals(3, graph.getConnectedComponent(graph.getById(6)).size());
        assertEquals(2, graph.getConnectedComponent(graph.getById(7)).size());
        assertEquals(2, graph.getConnectedComponent(graph.getById(8)).size());
        assertEquals(1, graph.getConnectedComponent(graph.getById(9)).size());
        assertEquals(1, graph.getConnectedComponent(graph.getById(0)).size());

        assertEquals(2, graph.calculateScoreOf(Affiliation.CARTHAGE));
        assertEquals(6, graph.calculateScoreOf(Affiliation.ROME));
    }
}
