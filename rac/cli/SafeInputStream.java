/*
 * Copyright 2014 Julian Andres Klode <klode@mathematik.uni-marburg.de>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package rac.cli;

import java.io.*;

/**
 * Helper class to ensure that lines do not exceed a maximum length
 *
 * This ensures that Scanner does not read endlessly from a source in some
 * cases.
 */
class SafeInputStream extends InputStream {
    /** Maximum line length */
    private static final int MAX_LINELEN = 8192;
    /** Stream to wrap */
    private InputStream stream;
    /** Number of characters in the current line */
    private int charCount = 0;

    /**
     * We cannot raise an IOException here, as Scanner will catch it
     */
    private class LineTooLongException extends RuntimeException {
        public LineTooLongException(String message) {
            super(message);
        }
    }

    public SafeInputStream(InputStream stream) {
        this.stream = stream;
    }

    @Override
    public int read() throws IOException {
        int c = stream.read();

        if (c == '\r' || c == '\n')
            charCount = 0;
        else
            charCount++;

        if (charCount > MAX_LINELEN) {
            throw new LineTooLongException("Maximum line length of "
                                           + MAX_LINELEN
                                           + " characters exceeded");
        }

        return c;
    }

    @Override
    public int read(byte[] b, int off, int len) throws IOException {
        int count = stream.read(b, off, len);
        if (count != -1) {
            for (int i = off; i < off + count; i++) {
                if (b[i] == '\n' || b[i] == '\r')
                    charCount = 0;
                else
                    charCount++;
            }

            if (charCount > MAX_LINELEN) {
                throw new LineTooLongException("Maximum line length of "
                                               + MAX_LINELEN
                                               + " characters exceeded");
            }
        }

        return count;
    }
}
