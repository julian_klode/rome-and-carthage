/*
 * Copyright 2014 Julian Andres Klode <klode@mathematik.uni-marburg.de>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package rac.cli;


import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;
import java.util.HashSet;

import rac.graph.*;

public class Program {
    Player[] players;
    boolean activePlayer;
    public Graph graph;
    HashSet<String> previousBoards = new HashSet<String>();
    int forfeitCount = 0;
    boolean console;

    public enum Result {
        /** The game ended */
        END_OF_GAME,
        /** Not a forfeit (for debugging purposes): Invalid input */
        INVALID_INPUT,
        /** Forfeit: Same board reached as previously */
        ILLEGAL_MOVE,
        /** Forfeit: Invalid player */
        INVALID_PLAYER,
        /** Forfeit: The move counts as forfeit, was probably illegal */
        FORFEIT,
        /** Continue playing the game */
        CONTINUE;

        public String toDescriptiveString() {
            switch (this) {
            case END_OF_GAME:
                return "End of game";
            case INVALID_INPUT:
                return "Invalid input (ignored)";
            case ILLEGAL_MOVE:
                return "The move would cause a repeated affiliation";
            case FORFEIT:
                return "The move had no effect on the map";
            case INVALID_PLAYER:
                return "The player given was invalid";
            case CONTINUE:
                return "Continue";
            }
            return "INVALID";
        }
    }

    /**
     * Load a program from a file
     */
    public Program(String path) throws FileNotFoundException {
        this(path, true);
    }


    /**
     * Load a program from a a path
     *
     * @param console Whether output should be written to the console.
     */
    public Program(String path, boolean console) throws FileNotFoundException {
        this(new Scanner(new File(path)), console);
    }

    /**
     * Load a program from a scanner
     *
     * @param console Whether output should be written to the console.
     */
    public Program(Scanner scanner, boolean console) {
        graph = new Graph(scanner);
        players = new Player[] {
            new ConsolePlayer(this, Affiliation.ROME),
            new ConsolePlayer(this, Affiliation.CARTHAGE)
        };
        activePlayer = false;
        this.console = console;
    }

    /**
     * Set a player
     */
    public Program setPlayer(Player player) {
        if (player.affiliation == Affiliation.ROME) {
            players[0] = player;
        } else if (player.affiliation == Affiliation.CARTHAGE) {
            players[1] = player;
        }
        return this;
    }

    /** Get the active player */
    public Player getPlayer() {
        return players[activePlayer ? 1 : 0];
    }

    /** Get the inactive player */
    public Player getOtherPlayer() {
        return players[activePlayer ? 0 : 1];
    }

    /** Switch the active player */
    private void switchPlayer() {
        activePlayer = !activePlayer;
    }

    /** Check whether a graph is already known */
    public boolean isGraphKnown(Graph graph) {
        return previousBoards.contains(graph.toString());
    }

    /** Check whether the game is over */
    public boolean isOver() {
        return forfeitCount >= 2;
    }

    /** The game is over */
    public void setIsOver() {
        forfeitCount = 2;
    }

    /**
     * Make a move, throws an exception on invalid formatted input.
     */
    public Result move(Move move) {
        assert move != null;
        Graph newGraph = graph.makeMove(move);

        if (move.player != getPlayer().affiliation) {
            forfeitCount++;
            switchPlayer();
            return Result.INVALID_PLAYER;
        }
        switchPlayer();


        if (newGraph != graph) {
            if (isGraphKnown(newGraph)) {
                forfeitCount++;
                if (forfeitCount == 2)
                    return Result.END_OF_GAME;
                return Result.ILLEGAL_MOVE;
            }

            previousBoards.add(graph.toString());

            graph = newGraph;
            forfeitCount = 0;

            return Result.CONTINUE;
        }

        forfeitCount++;
        if (forfeitCount == 2)
            return Result.END_OF_GAME;

        return Result.FORFEIT;
    }

    /**
     * Make a move.
     *
     * Cannot throw an exception.
     */
    public Result move(String command) {
        try {
            return move(new Move(command));
        } catch (Exception e) {
            System.err.println("Received exception: " + e);
            return Result.INVALID_INPUT;
        }
    }

    /** Returns true if a next move should be made */
    public Result playOne() throws IOException {
        Move m = getPlayer().calculateMove();

        if (console && !getPlayer().isInteractive())
            System.out.println("> " + m.toString());

        Result r = move(m);

        if (console)
            System.out.println(graph.toString() + " (" + r.toDescriptiveString() + ")");

        return r;
    }

    /**
     *
     *
     * @todo Better error handling
     */
    public void play() throws IOException {
        while (playOne() != Result.END_OF_GAME)
            ;
    }

    /**
     *
     *
     * @todo Better error handling
     */
    public static void main(String[] args) throws IOException {
        Program program = new Program(args[0], true);
        program.play();
    }
}
