/*
 * Copyright 2014 Julian Andres Klode <klode@mathematik.uni-marburg.de>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package rac.graph.tests;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Scanner;

import static org.junit.Assert.*;
import org.junit.Test;

import rac.graph.*;

/**
 * Test various utility functions
 */
public class UtilTest {
    @Test
    public void testCorrectEnemy() {
        assertEquals(Affiliation.ROME, Affiliation.CARTHAGE.getEnemy());
        assertEquals(Affiliation.CARTHAGE, Affiliation.ROME.getEnemy());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNeutralHasNoEnemy() {
        Affiliation.NEUTRAL.getEnemy();
    }

    @Test
    public void testAffiliationString() {
        assertSame(Affiliation.NEUTRAL, Affiliation.fromString("N"));
        assertSame(Affiliation.ROME, Affiliation.fromString("R"));
        assertSame(Affiliation.CARTHAGE, Affiliation.fromString("C"));
    }
    @Test
    public void testAffiliationToString() {
        assertEquals("N", Affiliation.NEUTRAL.toString());
        assertEquals("R", Affiliation.ROME.toString());
        assertEquals("C", Affiliation.CARTHAGE.toString());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testAffiliationStringInvalid() {
        Affiliation.fromString("CX");
    }

    @Test
    public void testScoreOfNeutral() throws FileNotFoundException {
        Graph graph = new Graph(new Scanner(new File("examples/delta.txt")));
        try {
            graph.calculateScoreOf(Affiliation.NEUTRAL);
        } catch(IllegalArgumentException e) {
            return;
        }
        fail("Neutral has a score");
    }
    @Test
    public void testRemove() throws FileNotFoundException {
        Graph graph = new Graph(new Scanner(new File("examples/delta.txt")));
        try {
            graph.remove(graph.getById(0));
        } catch(UnsupportedOperationException e) {
            return;
        }
        fail("Graph can remove stuff");
    }

    @Test
    public void testRemoveIteratorGraph() throws FileNotFoundException {
        Graph graph = new Graph(new Scanner(new File("examples/delta.txt")));
        Iterator<Vertex> iterator = graph.iterator();
        iterator.next();
        try {
            iterator.remove();
        } catch(UnsupportedOperationException e) {
            return;
        }
        fail("Graph can remove stuff");
    }
    @Test
    public void testRemoveIteratorSet() throws FileNotFoundException {
        Graph graph = new Graph(new Scanner(new File("examples/delta.txt")));
        VertexSet set = new VertexSet(Arrays.asList(new Vertex[] {graph.getById(0), graph.getById(2)}));
        Iterator<Vertex> iterator = set.iterator();
        iterator.next();
        iterator.remove();
        assertTrue(iterator.hasNext());
        assertEquals(2, iterator.next().getId());
        assertFalse(iterator.hasNext());


    }
}
