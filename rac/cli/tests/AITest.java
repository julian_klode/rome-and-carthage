/*
 * Copyright 2014 Julian Andres Klode <klode@mathematik.uni-marburg.de>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package rac.cli.tests;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import static org.junit.Assert.*;
import org.junit.Test;

import rac.cli.*;
import rac.graph.*;

public class AITest {

    @Test
    public void testSlothSloth() throws Exception {
        Program program = new Program("examples/alpha.txt", false);
        Player player1 = new SlothPlayer(program, Affiliation.ROME);
        Player player2 = new SlothPlayer(program, Affiliation.CARTHAGE);
        program.setPlayer(player1);
        program.setPlayer(player2);
        program.play();
        assertEquals(4, program.graph.calculateScoreOf(player1.getAffiliation()));
        assertEquals(0, program.graph.calculateScoreOf(player2.getAffiliation()));

        program = new Program("examples/beta.txt", false);
        player1 = new SlothPlayer(program, Affiliation.ROME);
        player2 = new SlothPlayer(program, Affiliation.CARTHAGE);
        program.setPlayer(player1);
        program.setPlayer(player2);
        program.play();
        assertEquals(10, program.graph.calculateScoreOf(player1.getAffiliation()));
        assertEquals(0, program.graph.calculateScoreOf(player2.getAffiliation()));

        program = new Program("examples/gamma.txt", false);
        player1 = new SlothPlayer(program, Affiliation.ROME);
        player2 = new SlothPlayer(program, Affiliation.CARTHAGE);
        program.setPlayer(player1);
        program.setPlayer(player2);
        program.play();
        assertEquals(4, program.graph.calculateScoreOf(player1.getAffiliation()));
        assertEquals(0, program.graph.calculateScoreOf(player2.getAffiliation()));

        program = new Program("examples/delta.txt", false);
        player1 = new SlothPlayer(program, Affiliation.ROME);
        player2 = new SlothPlayer(program, Affiliation.CARTHAGE);
        program.setPlayer(player1);
        program.setPlayer(player2);
        assertEquals("NNNNRRRNNC", program.graph.toString());
        assertEquals(Program.Result.FORFEIT, program.playOne());  // R
        assertEquals("NNNNRRRNNC", program.graph.toString());
        assertEquals(Program.Result.CONTINUE, program.playOne());  // C
        assertEquals("CNNNRRRNNC", program.graph.toString());
        assertEquals(Program.Result.CONTINUE, program.playOne());  // R
        assertEquals("CRNNRRRNNC", program.graph.toString());
        assertEquals(Program.Result.CONTINUE, program.playOne());  // C
        assertEquals("CRCNRRRNNC", program.graph.toString());
        assertEquals(Program.Result.CONTINUE, program.playOne());  // R
        assertEquals("CRNRRRRNNC", program.graph.toString());
        assertEquals(Program.Result.FORFEIT, program.playOne());  // C
        assertEquals("CRNRRRRNNC", program.graph.toString());
        assertEquals(Program.Result.CONTINUE, program.playOne());  // R
        assertEquals("CRRRRRRNNC", program.graph.toString());
        assertEquals(Program.Result.CONTINUE, program.playOne());  // C
        assertEquals("CRRRRRRCNC", program.graph.toString());
        assertEquals(Program.Result.CONTINUE, program.playOne());  // R 8
        assertEquals("NRRRRRRNRN", program.graph.toString());
        assertEquals(Program.Result.CONTINUE, program.playOne());  // C 0
        assertEquals("CRRRRRRNRN", program.graph.toString());
        assertEquals(Program.Result.CONTINUE, program.playOne());  // R 7
        assertEquals("CRRRRRRRRN", program.graph.toString());
        assertEquals(Program.Result.CONTINUE, program.playOne());  // C 9
        assertEquals("CNNNNNNNNC", program.graph.toString());
        assertEquals(Program.Result.CONTINUE, program.playOne());  // R 1
        assertEquals("CRNNNNNNNC", program.graph.toString());
        assertEquals(Program.Result.CONTINUE, program.playOne());  // C 2
        assertEquals("CRCNNNNNNC", program.graph.toString());
        assertEquals(Program.Result.CONTINUE, program.playOne());  // R 3
        assertEquals("CRCRNNNNNC", program.graph.toString());
        assertEquals(Program.Result.CONTINUE, program.playOne());  // C 4
        assertEquals("CRCRCNNNNC", program.graph.toString());
        assertEquals(Program.Result.CONTINUE, program.playOne());  // R 5
        assertEquals("CRCRCRNNNC", program.graph.toString());
        assertEquals(Program.Result.CONTINUE, program.playOne());  // C 6
        assertEquals("CRCRCRCNNC", program.graph.toString());
        assertEquals(Program.Result.CONTINUE, program.playOne());  // R 7
        assertEquals("CRCRCRCRNC", program.graph.toString());
        assertEquals(Program.Result.CONTINUE, program.playOne());  // C 8
        assertEquals("CNCNCNCNCC", program.graph.toString());
        assertEquals(Program.Result.CONTINUE, program.playOne());  // R 1
        assertEquals("CRCNCNCNCC", program.graph.toString());
        assertEquals(Program.Result.CONTINUE, program.playOne());  // C 3
        assertEquals("CNCCCNCNCC", program.graph.toString());
        assertEquals(Program.Result.FORFEIT, program.playOne());  // R 1
        assertEquals(Program.Result.CONTINUE, program.playOne());  // C 1
        assertEquals("CCCCCNCNCC", program.graph.toString());
        assertEquals(5, program.getPlayer().calculateMove().cityToAnnex);
        assertEquals(Program.Result.FORFEIT, program.playOne());  // R 5
        assertEquals("CCCCCNCNCC", program.graph.toString());
        assertEquals(5, program.getPlayer().calculateMove().cityToAnnex);
        assertEquals(Program.Result.CONTINUE, program.playOne());  // C 5
        assertEquals("CCCCCCCNCC", program.graph.toString());
        assertEquals(Program.Result.CONTINUE, program.playOne());  // R 7
        assertEquals("NNNNNNNRNN", program.graph.toString());
        assertEquals(Program.Result.CONTINUE, program.playOne());  // C 0
        assertEquals("CNNNNNNRNN", program.graph.toString());
        assertEquals(Program.Result.CONTINUE, program.playOne());  // R 1
        assertEquals("CRNNNNNRNN", program.graph.toString());

        /* Let's end this */
        program.play();
        assertEquals(0, program.graph.calculateScoreOf(player1.getAffiliation()));
        assertEquals(10, program.graph.calculateScoreOf(player2.getAffiliation()));
    }

    @Test
    public void testWaspWasp() throws Exception {
        Program program = new Program("examples/alpha.txt", false);
        Player player1 = new WaspPlayer(program, Affiliation.ROME);
        Player player2 = new WaspPlayer(program, Affiliation.CARTHAGE);
        program.setPlayer(player1);
        program.setPlayer(player2);
        program.play();
        assertEquals(1, program.graph.calculateScoreOf(player1.getAffiliation()));
        assertEquals(1, program.graph.calculateScoreOf(player2.getAffiliation()));

        program = new Program("examples/beta.txt", false);
        player1 = new WaspPlayer(program, Affiliation.ROME);
        player2 = new WaspPlayer(program, Affiliation.CARTHAGE);
        program.setPlayer(player1);
        program.setPlayer(player2);
        program.play();
        assertEquals(2, program.graph.calculateScoreOf(player1.getAffiliation()));
        assertEquals(2, program.graph.calculateScoreOf(player2.getAffiliation()));

        program = new Program("examples/gamma.txt", false);
        player1 = new WaspPlayer(program, Affiliation.ROME);
        player2 = new WaspPlayer(program, Affiliation.CARTHAGE);
        program.setPlayer(player1);
        program.setPlayer(player2);
        program.play();
        assertEquals(1, program.graph.calculateScoreOf(player1.getAffiliation()));
        assertEquals(1, program.graph.calculateScoreOf(player2.getAffiliation()));

        program = new Program("examples/delta.txt", false);
        player1 = new WaspPlayer(program, Affiliation.ROME);
        player2 = new WaspPlayer(program, Affiliation.CARTHAGE);
        program.setPlayer(player1);
        program.setPlayer(player2);
        program.play();
        assertEquals(6, program.graph.calculateScoreOf(player1.getAffiliation()));
        assertEquals(2, program.graph.calculateScoreOf(player2.getAffiliation()));
    }
    @Test
    public void testWaspSloth() throws Exception {
        Program program = new Program("examples/alpha.txt", false);
        Player player1 = new WaspPlayer(program, Affiliation.ROME);
        Player player2 = new SlothPlayer(program, Affiliation.CARTHAGE);
        program.setPlayer(player1);
        program.setPlayer(player2);
        program.play();
        assertEquals(0, program.graph.calculateScoreOf(player1.getAffiliation()));
        assertEquals(4, program.graph.calculateScoreOf(player2.getAffiliation()));

        program = new Program("examples/beta.txt", false);
        player1 = new WaspPlayer(program, Affiliation.ROME);
        player2 = new SlothPlayer(program, Affiliation.CARTHAGE);
        program.setPlayer(player1);
        program.setPlayer(player2);
        program.play();
        assertEquals(0, program.graph.calculateScoreOf(player1.getAffiliation()));
        assertEquals(10, program.graph.calculateScoreOf(player2.getAffiliation()));

        program = new Program("examples/gamma.txt", false);
        player1 = new WaspPlayer(program, Affiliation.ROME);
        player2 = new SlothPlayer(program, Affiliation.CARTHAGE);
        program.setPlayer(player1);
        program.setPlayer(player2);
        program.play();
        assertEquals(0, program.graph.calculateScoreOf(player1.getAffiliation()));
        assertEquals(4, program.graph.calculateScoreOf(player2.getAffiliation()));

        program = new Program("examples/delta.txt", false);
        player1 = new WaspPlayer(program, Affiliation.ROME);
        player2 = new SlothPlayer(program, Affiliation.CARTHAGE);
        program.setPlayer(player1);
        program.setPlayer(player2);
        program.play();
        assertEquals(3, program.graph.calculateScoreOf(player1.getAffiliation()));
        assertEquals(4, program.graph.calculateScoreOf(player2.getAffiliation()));
    }
    @Test
    public void testSlothWasp() throws Exception {
        Program program = new Program("examples/alpha.txt", false);
        Player player1 = new WaspPlayer(program, Affiliation.CARTHAGE);
        Player player2 = new SlothPlayer(program, Affiliation.ROME);
        program.setPlayer(player1);
        program.setPlayer(player2);
        program.play();
        assertEquals(0, program.graph.calculateScoreOf(player1.getAffiliation()));
        assertEquals(4, program.graph.calculateScoreOf(player2.getAffiliation()));

        program = new Program("examples/beta.txt", false);
        player1 = new WaspPlayer(program, Affiliation.CARTHAGE);
        player2 = new SlothPlayer(program, Affiliation.ROME);
        program.setPlayer(player1);
        program.setPlayer(player2);
        program.play();
        assertEquals(1, program.graph.calculateScoreOf(player1.getAffiliation()));
        assertEquals(7, program.graph.calculateScoreOf(player2.getAffiliation()));

        program = new Program("examples/gamma.txt", false);
        player1 = new WaspPlayer(program, Affiliation.CARTHAGE);
        player2 = new SlothPlayer(program, Affiliation.ROME);
        program.setPlayer(player1);
        program.setPlayer(player2);
        program.play();
        assertEquals(0, program.graph.calculateScoreOf(player1.getAffiliation()));
        assertEquals(4, program.graph.calculateScoreOf(player2.getAffiliation()));

        program = new Program("examples/delta.txt", false);
        player1 = new WaspPlayer(program, Affiliation.CARTHAGE);
        player2 = new SlothPlayer(program, Affiliation.ROME);
        program.setPlayer(player1);
        program.setPlayer(player2);
        program.play();
        assertEquals(1, program.graph.calculateScoreOf(player1.getAffiliation()));
        assertEquals(7, program.graph.calculateScoreOf(player2.getAffiliation()));
    }
    @Test
    public void testSlothLawyer() throws Exception {
        Program program = new Program("examples/alpha.txt", false);
        Player carthage = new LawyerPlayer(program, Affiliation.CARTHAGE);
        Player rome = new SlothPlayer(program, Affiliation.ROME);
        program.setPlayer(rome);
        program.setPlayer(carthage);
        assertEquals("R 1", program.getPlayer().calculateMove().toString());
        assertEquals(Program.Result.CONTINUE, program.playOne());
        assertEquals("NRNR", program.graph.toString());

        assertEquals("C 2", program.getPlayer().calculateMove().toString());
        assertEquals(Program.Result.CONTINUE, program.playOne());
        assertEquals("NRCN", program.graph.toString());

        assertEquals("R 0", program.getPlayer().calculateMove().toString());
        assertEquals(Program.Result.CONTINUE, program.playOne());
        assertEquals("NNCN", program.graph.toString()); /* Suicide */

        assertEquals("C 0", program.getPlayer().calculateMove().toString());
        assertEquals(Program.Result.CONTINUE, program.playOne());
        assertEquals("CNCN", program.graph.toString());

        assertEquals("R 1", program.getPlayer().calculateMove().toString());
        assertEquals("NRCN", program.graph.makeMove(new Move("R 1")).toString());
        assertEquals(Program.Result.ILLEGAL_MOVE, program.playOne());
        assertEquals("CNCN", program.graph.toString());

        assertEquals("C 1", program.getPlayer().calculateMove().toString());
        assertEquals(Program.Result.CONTINUE, program.playOne());
        assertEquals("CCCN", program.graph.toString());

        assertEquals("R 3", program.getPlayer().calculateMove().toString());
        assertEquals(Program.Result.CONTINUE, program.playOne());
        assertEquals("NNNR", program.graph.toString());

        assertEquals("C 1", program.getPlayer().calculateMove().toString());
        assertEquals(Program.Result.CONTINUE, program.playOne());
        assertEquals("NCNR", program.graph.toString());

        assertEquals("R 0", program.getPlayer().calculateMove().toString());
        assertEquals(Program.Result.FORFEIT, program.playOne()); /* Suicide */
        assertEquals("NCNR", program.graph.toString());

        assertEquals("C 0", program.getPlayer().calculateMove().toString());
        assertEquals(Program.Result.CONTINUE, program.playOne());
        assertEquals("CCNR", program.graph.toString());

        assertEquals("R 2", program.getPlayer().calculateMove().toString());
        assertEquals(Program.Result.CONTINUE, program.playOne());
        assertEquals("NNRR", program.graph.toString());

        assertEquals("C 0", program.getPlayer().calculateMove().toString());
        assertEquals(Program.Result.CONTINUE, program.playOne());
        assertEquals("CNRR", program.graph.toString());

        assertEquals("R 1", program.getPlayer().calculateMove().toString());
        assertEquals(Program.Result.CONTINUE, program.playOne());
        assertEquals("NRRR", program.graph.toString());

        assertEquals("C 0", program.getPlayer().calculateMove().toString());
        assertEquals(Program.Result.CONTINUE, program.playOne());
        assertEquals("CNNN", program.graph.toString());

        assertEquals("R 1", program.getPlayer().calculateMove().toString());
        assertEquals(Program.Result.CONTINUE, program.playOne());
        assertEquals("NRNN", program.graph.toString());

        assertEquals("C 3", program.getPlayer().calculateMove().toString());
        assertEquals(Program.Result.CONTINUE, program.playOne());
        assertEquals("NRNC", program.graph.toString());

        assertEquals("R 0", program.getPlayer().calculateMove().toString());
        assertEquals(Program.Result.CONTINUE, program.playOne());
        assertEquals("RRNC", program.graph.toString());

        assertEquals("C 2", program.getPlayer().calculateMove().toString());
        assertEquals(Program.Result.CONTINUE, program.playOne());
        assertEquals("NNCC", program.graph.toString());

        assertEquals("R 0", program.getPlayer().calculateMove().toString());
        assertEquals(Program.Result.CONTINUE, program.playOne());
        assertEquals("RNCC", program.graph.toString());

        assertEquals("C 1", program.getPlayer().calculateMove().toString());
        assertEquals(Program.Result.CONTINUE, program.playOne());
        assertEquals("NCCC", program.graph.toString());

        assertEquals("R 0", program.getPlayer().calculateMove().toString());
        assertEquals(Program.Result.CONTINUE, program.playOne());
        assertEquals("RNNN", program.graph.toString());

        assertEquals("C 1", program.getPlayer().calculateMove().toString());
        assertEquals(Program.Result.CONTINUE, program.playOne());
        assertEquals("NCNN", program.graph.toString());

        assertEquals("R 0", program.getPlayer().calculateMove().toString());
        assertEquals(Program.Result.FORFEIT, program.playOne());  /* Suicide */
        assertEquals("NCNN", program.graph.toString());

        assertEquals("C 0", program.getPlayer().calculateMove().toString());
        assertEquals(Program.Result.CONTINUE, program.playOne());
        assertEquals("CCNN", program.graph.toString());

        assertEquals("R 2", program.getPlayer().calculateMove().toString());
        assertEquals(Program.Result.CONTINUE, program.playOne());
        assertEquals("NNRN", program.graph.toString());

        assertEquals("C 0", program.getPlayer().calculateMove().toString());
        assertEquals(Program.Result.CONTINUE, program.playOne());
        assertEquals("CNRN", program.graph.toString());

        assertEquals("R 1", program.getPlayer().calculateMove().toString());
        assertEquals(Program.Result.CONTINUE, program.playOne());
        assertEquals("NRRN", program.graph.toString());

        assertEquals("C X", program.getPlayer().calculateMove().toString());
        assertEquals(Program.Result.FORFEIT, program.playOne());
        assertEquals("NRRN", program.graph.toString());

        assertEquals("R 0", program.getPlayer().calculateMove().toString());
        assertEquals(Program.Result.CONTINUE, program.playOne());
        assertEquals("RRRN", program.graph.toString());

        assertEquals("C 3", program.getPlayer().calculateMove().toString());
        assertEquals(Program.Result.CONTINUE, program.playOne());
        assertEquals("NNNC", program.graph.toString());

        assertEquals("R 0", program.getPlayer().calculateMove().toString());
        assertEquals(Program.Result.CONTINUE, program.playOne());
        assertEquals("RNNC", program.graph.toString());

        assertEquals("C 1", program.getPlayer().calculateMove().toString());
        assertEquals(Program.Result.CONTINUE, program.playOne());
        assertEquals("NCNC", program.graph.toString());

        assertEquals("R 0", program.getPlayer().calculateMove().toString());
        assertEquals(Program.Result.FORFEIT, program.playOne()); /* Suicide */
        assertEquals("NCNC", program.graph.toString());

        assertEquals("C 0", program.getPlayer().calculateMove().toString());
        assertEquals(Program.Result.CONTINUE, program.playOne());
        assertEquals("CCNC", program.graph.toString());

        assertEquals("R 2", program.getPlayer().calculateMove().toString());
        assertEquals("NNRN", program.graph.makeMove(new Move("R 2")).toString());
        assertEquals(Program.Result.ILLEGAL_MOVE, program.playOne());
        assertEquals("CCNC", program.graph.toString());

        assertEquals("C 2", program.getPlayer().calculateMove().toString());
        assertEquals("NNNN", program.graph.makeMove(new Move("C 2")).toString());
        assertEquals(Program.Result.CONTINUE, program.playOne());
        assertEquals("NNNN", program.graph.toString());

        assertEquals("R 0", program.getPlayer().calculateMove().toString());
        assertEquals(Program.Result.ILLEGAL_MOVE, program.playOne());
        assertEquals("NNNN", program.graph.toString());

        assertEquals("C X", program.getPlayer().calculateMove().toString());
        assertEquals(Program.Result.END_OF_GAME, program.playOne());
        assertEquals("NNNN", program.graph.toString());
    }
}
