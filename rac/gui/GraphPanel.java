/*
 * Copyright 2014 Julian Andres Klode <klode@mathematik.uni-marburg.de>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package rac.gui;

import javax.swing.*;
import java.awt.*;

import rac.graph.*;

class GraphPanel extends JPanel {
    private final Graph graph;

    public GraphPanel(Graph graph) {
        super();
        this.graph = graph;
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D) g;
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                            RenderingHints.VALUE_ANTIALIAS_ON);

        for (Vertex a : graph) {
            for (Vertex b: graph.getNeighbourhood(a)) {
                if (a.getId() < b.getId()) {
                    g.drawLine(a.getX() + GUI.BUTTON_WIDTH / 2,
                               a.getY() + GUI.BUTTON_HEIGHT / 2,
                               b.getX() + GUI.BUTTON_WIDTH / 2,
                               b.getY() + GUI.BUTTON_HEIGHT / 2);
                }
            }

        }
    }
}
