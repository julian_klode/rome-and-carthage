/*
 * Copyright 2014 Julian Andres Klode <klode@mathematik.uni-marburg.de>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package rac.cli;

import java.util.HashSet;

import rac.graph.*;

/**
 * Interface for Players, both AI and persons
 */
public class SlothPlayer extends Player {
    public SlothPlayer(Program program, Affiliation affiliation) {
        super(program, affiliation);
    }

    /**
     * Calculate the next move to make.
     */
    public Move calculateMove() {
        for (Vertex v : program.graph) {
            if (v.getAffiliation() == Affiliation.NEUTRAL)
                return new Move(affiliation, v.getId());

        }
        return new Move(affiliation, Move.FORFEIT);
    }
}
