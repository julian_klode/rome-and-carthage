/*
 * Copyright 2014 Julian Andres Klode <klode@mathematik.uni-marburg.de>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package rac.cli;

import java.util.HashSet;

import rac.graph.*;

/**
 * Interface for Players, both AI and persons
 */
public abstract class Player {
    protected Program program;
    protected Affiliation affiliation;

    public Player(Program program, Affiliation affiliation) {
        this.program = program;
        this.affiliation = affiliation;
    }

    /** Get affiliation */
    public Affiliation getAffiliation() {
        return affiliation;
    }

    /**
     * Calculate the next move to make.
     */
    public abstract Move calculateMove();

    /** Whether we ask on console */
    public boolean isInteractive() {
        return false;
    }
}
