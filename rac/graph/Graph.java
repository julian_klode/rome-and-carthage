/*
 * Copyright 2014 Julian Andres Klode <klode@mathematik.uni-marburg.de>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package rac.graph;

import java.io.PrintStream;
import java.util.Collection;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * A graph contains vertices and (unnamed) edges.
 *
 * Vertices are represented by Vertex objects, edges are represented as
 * references between vertices.
 */
public class Graph extends VertexSet {
    /** Store the edges as a bitmap */
    private java.util.BitSet edges;

    /**
     * Joyless constructor
     */
    private Graph(Iterable<Vertex> vertices, java.util.BitSet edges) {
        super(vertices);
        this.edges = edges;
    }

    /**
     * Construct a new Graph from a Scanner
     */
    public Graph(Scanner scanner) {
        super(scanner.nextInt());
        edges = new java.util.BitSet(vertices.length * vertices.length);

        scanner.nextLine();

        for (int i = 0; i < vertices.length; i++) {
            Vertex vertex = new Vertex(scanner);
            vertices[vertex.getId()] = vertex;
            scanner.nextLine();
        }

        while (scanner.hasNext("E")) {
            String type = scanner.next("E");

            int x = scanner.nextInt();
            int y = scanner.nextInt();

            /* Connect edges */
            edges.set(x * vertices.length + y);
            edges.set(y * vertices.length + x);

            scanner.nextLine();
        }
    }

    /**
     * Remove a vertex from the set (unsupported)
     *
     * @throws UnsupportedOperationException
     */
    @Override
    public void remove(Vertex vertex) {
        throw new UnsupportedOperationException("Not supported");
    }

    /** Calculate the size of the graph. Special case O(1) here */
    @Override
    public int size() {
        return vertices.length;
    }

    /**
     * Calculate neighbourhood of the vertex.
     *
     * Contrary to what people expect, this is O(1).
     */
    public VertexSet getNeighbourhood(Vertex vertex) {
        VertexSet set = new VertexSet(vertices.length);
        for (int i = 0; i < vertices.length; i++) {
            if (edges.get(vertex.getId() * vertices.length + i))
                set.add(vertices[i]);
        }
        return set;
    }

    /** Calculate neighbourhood of the set, complexity O(n) */
    public VertexSet getNeighbourhood(VertexSet vertices) {
        VertexSet set = new VertexSet(this.vertices.length);

        for (Vertex v : vertices)
            set.addAll(getNeighbourhood(v));

        set.removeAll(vertices);

        return set;
    }

    /**
     * Calculate all connected component
     *
     * Those are all neighbours with the same affiliation, their neighbours
     * with the same affiliation, and so on.
     *
     * @param set The resulting set
     * @return The argument
     */
    private VertexSet addConnectedComponentTo(Vertex vertex, VertexSet set) {
        /* Prevent infinite recursion */
        if (set.contains(vertex))
            return set;

        set.add(vertex);

        for (Vertex v: getNeighbourhood(vertex))
            if (v.getAffiliation() == vertex.getAffiliation())
                addConnectedComponentTo(v, set);

        return set;
    }

    /**
     * Get all connected component of the vertex.
     */
    public VertexSet getConnectedComponent(Vertex v) {
        return addConnectedComponentTo(v, new VertexSet(vertices.length));
    }

    /**
     * Calculate the score of a player.
     *
     * This counts the vertices of all connected components that are surrounded
     * by vertices of the given affiliation and the vertices of the given
     * affiliation themselves.
     */
    public int calculateScoreOf(final Affiliation player) {
        if (player == Affiliation.NEUTRAL)
            throw new IllegalArgumentException("neutral is not a player");

        VertexSet counted = new VertexSet(vertices.length);
        for (Vertex v : this) {
            if (counted.contains(v))
                continue;

            if (v.getAffiliation() == player) {
                counted.add(v);
            } else if (v.getAffiliation() == Affiliation.NEUTRAL) {
                VertexSet component = getConnectedComponent(v);
                if (isSurroundedBy(component, player) &&
                        !getNeighbourhood(component).isEmpty())
                    counted.addAll(component);
            }
        }
        return counted.size();
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < vertices.length; i++)
            stringBuilder.append(vertices[i].getAffiliation().toString());

        return stringBuilder.toString();
    }

    /**
     * Check whether a set of vertices is surrounded
     */
    public boolean isSurroundedBy(VertexSet component, final Affiliation enemy) {
        final VertexSet neighbours = getNeighbourhood(component);
        final VertexSet.Visitor isEnemy = new VertexSet.Visitor() {
            public boolean visit(Vertex v) {
                return v.getAffiliation() == enemy;
            }
        };
        final VertexSet.Visitor isNeutral = new VertexSet.Visitor() {
            public boolean visit(Vertex v) {
                return v.getAffiliation() == Affiliation.NEUTRAL;
            }
        };

        return !neighbours.forAny(isNeutral) && neighbours.forAll(isEnemy);
    }

    /**
     * Release a set of vertices (set Affiliation to neutral)
     */
    private void free(final VertexSet verticesToFree) {
        for (Vertex vertex : verticesToFree)
            vertices[vertex.getId()] = new Vertex(vertex, Affiliation.NEUTRAL);
    }

    /**
     * Release a set of vertices (set Affiliation to neutral)
     */
    private void freeSurroundedNeighbours(final Vertex city) {
        VertexSet myComponent = getConnectedComponent(city);

        for (Vertex vertex : getNeighbourhood(myComponent)) {
            /* Already freed, ignore it */
            if (vertex.getAffiliation() == Affiliation.NEUTRAL)
                continue;
            VertexSet neighbours = getConnectedComponent(vertex);
            if (isSurroundedBy(neighbours, vertex.getAffiliation().getEnemy()))
                free(neighbours);
        }

    }

    /**
     * Make a single move, returns resulting graph Graph.
     *
     * @returns A new graph, if the move was allowed and changed anything, or
     *          this.
     */
    public Graph makeMove(Move move) {
        /* We'd change nothing, don't do anything further */
        if (move.cityToAnnex == Move.FORFEIT)
            return this;
        if (getById(move.cityToAnnex).getAffiliation() == move.player ||
                getById(move.cityToAnnex).getAffiliation() != Affiliation.NEUTRAL)
            return this;

        /* Clone the graph and make the move */
        Graph graph = new Graph(this, edges);

        /* Hey, nothing changed! */
        if (!graph.makeMoveModify(move) || graph.toString().equals(toString()))
            return this;

        return graph;
    }

    private boolean makeMoveModify(Move move) {
        /* Take over the city */
        vertices[move.cityToAnnex] = new Vertex(vertices[move.cityToAnnex],
                                                move.player);

        Vertex annexed = vertices[move.cityToAnnex];

        /* Free surrounded neighbours first */
        freeSurroundedNeighbours(annexed);

        /* Now, our component may be surrounded. If that is the case, we
         * immediately lose it */
        VertexSet annexedComponent = getConnectedComponent(annexed);
        if (isSurroundedBy(annexedComponent, move.player.getEnemy()))
            free(annexedComponent);

        return true;
    }

    /**
     * Print the graph to the given stream
     *
     * This uses the official graph format, the same one parsed by the
     * constructor.
     *
     * @param stream Stream to print to
     */
    public void printToStream(PrintStream stream, String lineEnding) {
        stream.printf("%d%s", vertices.length, lineEnding);

        for (Vertex v : this) {
            stream.printf("V %d %s %d %d%s", v.getId(), v.getAffiliation(),
                          v.getX(), v.getY(), lineEnding);
        }

        for (int i = 0; i < vertices.length; i++) {
            for (int j = i + 1; j > i && j < vertices.length; j++) {
                if (edges.get(i * vertices.length + j))
                    stream.printf("E %d %d%s", i, j, lineEnding);
            }
        }
    }
}
