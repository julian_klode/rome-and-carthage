/*
 * Copyright 2014 Julian Andres Klode <klode@mathematik.uni-marburg.de>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package rac.graph.tests;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.InputMismatchException;

import static org.junit.Assert.*;
import org.junit.Test;

import rac.graph.*;

public class ImportTest {

    @Test
    public void testAlpha() throws FileNotFoundException {
        Graph graph = new Graph(new Scanner(new File("examples/alpha.txt")));

        assertEquals(graph.size(), 4);

        assertEquals(graph.getById(0).getId(), 0);
        assertEquals(graph.getById(1).getId(), 1);
        assertEquals(graph.getById(2).getId(), 2);
        assertEquals(graph.getById(3).getId(), 3);
        assertEquals(graph.getById(0).getAffiliation(), Affiliation.CARTHAGE);
        assertEquals(graph.getById(1).getAffiliation(), Affiliation.NEUTRAL);
        assertEquals(graph.getById(2).getAffiliation(), Affiliation.NEUTRAL);
        assertEquals(graph.getById(3).getAffiliation(), Affiliation.ROME);
        assertEquals(graph.getById(0).getX(), 350);
        assertEquals(graph.getById(1).getX(), 250);
        assertEquals(graph.getById(2).getX(), 350);
        assertEquals(graph.getById(3).getX(), 250);
        assertEquals(graph.getById(0).getY(), 125);
        assertEquals(graph.getById(1).getY(), 275);
        assertEquals(graph.getById(2).getY(), 275);
        assertEquals(graph.getById(3).getY(), 425);

        assertEquals(null, graph.getById(4));

        assertEquals("0 C", graph.getById(0).toString());
        assertEquals("1", graph.getById(1).toString());
        assertEquals("2", graph.getById(2).toString());
        assertEquals("3 R", graph.getById(3).toString());

        assertEquals(graph.toString(), "CNNR");
    }

    @Test(expected = InputMismatchException.class)
    public void testVertexImportFailsNoV()  {
        new Vertex(new Scanner("1 N 250 275"));
    }

    @Test(expected = InputMismatchException.class)
    public void testVertexImportFailsNoId()  {
        new Vertex(new Scanner("V N 250 275"));
    }

    @Test(expected = InputMismatchException.class)
    public void testVertexImportFailsNoAff()  {
        new Vertex(new Scanner("V 1 250 275"));
    }

}
