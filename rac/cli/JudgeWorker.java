/*
 * JudgeWorker.java - Judge Worker threads.
 *
 * Copyright 2014 Julian Andres Klode <klode@mathematik.uni-marburg.de>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package rac.cli;

import java.io.*;
import java.net.*;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.InputMismatchException;
import java.util.Scanner;

import rac.graph.*;
import rac.cli.Program;
import rac.cli.Player;

/**
 * Worker thread for a single connection
 *
 * The judge consists of two threads that read from/write to sockets. The main
 * thread hosts rome, and another thread hosts carthage.
 */
public class JudgeWorker extends Thread {
    Judge judge;
    Affiliation affiliation;

    Scanner source;
    PrintStream sink;

    public class InvalidMove extends Move {
        public String comment;
        public String move;

        public InvalidMove(Affiliation aff, String comment, String move) {
            super(aff, Move.FORFEIT);

            this.comment = comment;
            this.move = move;
        }
    }

    public JudgeWorker(Judge judge, Affiliation affiliation, Socket socket, Scanner source) throws IOException {
        this.judge = judge;
        this.affiliation = affiliation;
        this.sink = new PrintStream(socket.getOutputStream());
        this.source = source != null ? source : new java.util.Scanner(new SafeInputStream(socket.getInputStream()));
    }


    @Override
    public void run() {
        try {
            boolean cont = true;
            while (cont && source.hasNextLine()) {
                Move move = new Move(source.nextLine());
                if (move.player != affiliation) {
                    judge.queueMove(new InvalidMove(affiliation, "PROTOCOL_ERROR", move.toString()));
                    judge.closeConnections();
                    return;
                }

                judge.queueMove(move);
            }
            /* No lines to read */
            if (cont) {
                judge.queueMove(new InvalidMove(affiliation, "PROTOCOL_ERROR", "<drop connection>"));
                judge.closeConnections();
            }
        } catch (Exception e) {
            System.err.printf("[%s] Received exception", affiliation);
            e.printStackTrace(System.err);
            if (source.ioException() != null) {
                source.ioException().printStackTrace(System.err);
            }
            judge.queueMove(new InvalidMove(affiliation, "EXCEPTION", "<" + e.toString() + ">"));
            judge.closeConnections();
        }
    }
}
