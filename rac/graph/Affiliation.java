/*
 * Copyright 2014 Julian Andres Klode <klode@mathematik.uni-marburg.de>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package rac.graph;

import java.util.NoSuchElementException;

/**
 * Each vertex can be affiliated with one party.
 */
public enum Affiliation {
    /** Not affiliated */
    NEUTRAL,
    /** Affiliated with CARTHAGE */
    CARTHAGE,
    /** Affiliated with ROME */
    ROME;

    /**
     * Get the enemy of the affiliation
     *
     * @throws IllegalArgumentException If the affiliation is NEUTRAL.
     */
    public Affiliation getEnemy() {
        switch (this) {
        case ROME:
            return CARTHAGE;
        case CARTHAGE:
            return ROME;
        }
        throw new IllegalArgumentException("NEUTRAL has no enemy");
    }

    /**
     * Get an Affiliation from a String
     *
     * This shall be one of
     *      N, NEUTRAL,
     *      C, CARTHAGE,
     *      R, ROME
     *
     * @throws IllegalArgumentException for unknown strings
     */
    public static Affiliation fromString(String s) {
        if (s.equals("N"))
            return NEUTRAL;
        if (s.equals("C"))
            return CARTHAGE;
        if (s.equals("R"))
            return ROME;

        throw new IllegalArgumentException("Unknown Affiliation");
    }

    @Override
    public String toString() {
        switch (this) {
        case NEUTRAL:
            return "N";
        case ROME:
            return "R";
        case CARTHAGE:
            return "C";
        }
        throw new NoSuchElementException("Invalid enumeration value");
    }
};
