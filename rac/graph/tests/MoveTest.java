/*
 * Copyright 2014 Julian Andres Klode <klode@mathematik.uni-marburg.de>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package rac.graph.tests;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

import rac.graph.*;

public class MoveTest {
    Graph alpha;
    Graph delta;

    @Before
    public void setUp() throws FileNotFoundException {
        alpha = new Graph(new Scanner(new File("examples/alpha.txt")));
        delta = new Graph(new Scanner(new File("examples/delta.txt")));
    }

    @Test
    public void testSuccess() {
        Move m;

        m = new Move("R 15");
        assertEquals(m.toString(), "R 15");
        m = new Move("C 42");
        assertEquals(m.toString(), "C 42");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNeutral() {
        Move m = new Move("N 42");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNegative() {
        Move m = new Move("C -1");
    }

    @Test
    public void testCarthageMoves() {
        assertEquals("CNNR", alpha.toString());
        alpha = alpha.makeMove(new Move("C 1"));
        assertEquals("CCNR", alpha.toString());
        alpha = alpha.makeMove(new Move("C 2"));
        assertEquals("CCCN", alpha.toString());
        alpha = alpha.makeMove(new Move("C 3"));
        assertEquals("NNNN", alpha.toString());
        assertEquals(0, alpha.calculateScoreOf(Affiliation.CARTHAGE));
        assertEquals(0, alpha.calculateScoreOf(Affiliation.ROME));
    }

    @Test
    public void testMixedMoves() {
        assertEquals("CNNR", alpha.toString());
        alpha = alpha.makeMove(new Move("C 1"));
        assertEquals("CCNR", alpha.toString());
        alpha = alpha.makeMove(new Move("R 2"));
        assertEquals("NNRR", alpha.toString());
        alpha = alpha.makeMove(new Move("R 1"));
        assertEquals("NRRR", alpha.toString());
        alpha = alpha.makeMove(new Move("R 0"));
        assertEquals("NNNN", alpha.toString());
        assertEquals(alpha.calculateScoreOf(Affiliation.CARTHAGE), 0);
        assertEquals(alpha.calculateScoreOf(Affiliation.ROME), 0);
    }

    /**
     * If we annex a city in a component that is then surrounded by the
     * enemy, this basically  is a suicide.
     */
    @Test
    public void testSuicide() {
        assertEquals("NNNNRRRNNC", delta.toString());
        delta = delta.makeMove(new Move("R 7"));
        assertEquals("NNNNRRRRNC", delta.toString());
        delta = delta.makeMove(new Move("R 8"));
        assertEquals("NNNNRRRRRC", delta.toString());
        delta = delta.makeMove(new Move("R 3"));
        assertEquals("NNNRRRRRRC", delta.toString());
        delta = delta.makeMove(new Move("R 2"));
        assertEquals("NNRRRRRRRC", delta.toString());
        delta = delta.makeMove(new Move("R 1"));
        assertEquals("NNNNNNNNNC", delta.toString());
    }

    /**
     * Annexing a city of the enemy is not allowed
     */
    @Test
    public void testNonNeutral() {
        assertEquals("CNNR", alpha.toString());
        Graph newAlpha = alpha.makeMove(new Move("R 0"));
        assertEquals("CNNR", newAlpha.toString());
        assertSame(alpha, newAlpha);
    }

    @Test
    public void testNoChanges() {
        /* Keeping the same affiliation does not change anything */
        Graph alpha = this.alpha.makeMove(new Move("R 3"));
        assertSame(alpha, this.alpha);
        /* Here, 0 is surrounded by carthage, so if rome annexes it, it will
         * lose it immediately => The move has no effect */
        Graph delta = this.delta.makeMove(new Move("R 0"));
        assertSame(delta, this.delta);
    }

    /**
     * Test the examples from the milestone description
     */
    @Test
    public void testExamples() throws FileNotFoundException {
        rac.cli.MoveProgram prog;

        prog = new rac.cli.MoveProgram("examples/moves/3-1.txt");
        assertEquals("CNNR", prog.graph1.toString());
        prog.makeMove();
        assertEquals("CNRR", prog.graph2.toString());

        prog = new rac.cli.MoveProgram("examples/moves/3-2.txt");
        assertEquals("CNNR", prog.graph1.toString());
        prog.makeMove();
        assertEquals("CNCN", prog.graph2.toString());

        prog = new rac.cli.MoveProgram("examples/moves/3-3.txt");
        assertEquals("CNCN", prog.graph1.toString());
        prog.makeMove();
        assertEquals("CNCN", prog.graph2.toString());
        assertSame(prog.graph1, prog.graph2);

        prog = new rac.cli.MoveProgram("examples/moves/3-4.txt");
        assertEquals("CNCN", prog.graph1.toString());
        prog.makeMove();
        assertEquals("NRCN", prog.graph2.toString());

    }
}
