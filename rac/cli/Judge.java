/*
 * Copyright 2014 Julian Andres Klode <klode@mathematik.uni-marburg.de>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package rac.cli;

import java.io.*;
import java.net.*;
import java.text.SimpleDateFormat;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.Calendar;
import java.util.InputMismatchException;
import java.util.Scanner;

import rac.graph.*;
import rac.cli.Program;
import rac.cli.Player;

/**
 * Worker thread for a single connection
 *
 * The judge consists of two threads that read from/write to sockets. The main
 * thread hosts rome, and another thread hosts carthage.
 */
public class Judge {
    /* Timeout in seconds */
    private static final int TIMEOUT_SEC = 10;
    private static final int SHORT_CUT_MSEC = 60 * 1000;
    private static final int RETRY_WAIT_MSEC = 1000;
    /* Workers */
    private JudgeWorker rome;
    private long start;
    private JudgeWorker carthage;

    private rac.gui.GUI gui;

    /* Program */
    private Program program;
    private Program.Result result;

    private LinkedBlockingQueue<Move> moveQueue = new LinkedBlockingQueue<Move>();

    /* Logging support */
    private PrintStream log;

    /** Queue a single move. Thread-safe */
    public void queueMove(Move move) {
        moveQueue.add(move);
    }

    /** Close all connections. Thread-safe */
    public void closeConnections() {
        rome.sink.close();
        carthage.sink.close();
    }

    private void log(Affiliation affiliation, String comment, String move) {
        final Calendar cal = Calendar.getInstance();
        final SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss.S");
        log.printf("%s\t%s\t\"%s\"\t%s\n", getTag(affiliation),
                   sdf.format(cal.getTime()), comment, move);
        log.flush();
    }

    private String getTag(Affiliation affiliation) {
        switch (affiliation) {
        case ROME:
            return JudgeGetOpt.romeTag;
        case CARTHAGE:
            return JudgeGetOpt.carthageTag;
        default:
            throw new AssertionError("Should not be reached");
        }
    }

    private void lost(Affiliation affiliation, String reason) {
        System.out.println("Winner: " + affiliation.getEnemy());
        System.out.println("Looser: " + affiliation);
        if (reason != null)
            System.out.println("Reason: " + affiliation + " "  + reason);

        log(affiliation.getEnemy(), "WINNER", "Game won");
        closeConnections();
    }

    private void calculateScores() {
        System.out.println("========== End of game =============");
        int rScore = program.graph.calculateScoreOf(Affiliation.ROME);
        int cScore = program.graph.calculateScoreOf(Affiliation.CARTHAGE);
        System.out.println("Rome: " + rScore);
        System.out.println("Chartage: " + cScore);

        if (rScore == cScore) {
            log(Affiliation.ROME, "TIE", "");
            log(Affiliation.CARTHAGE, "TIE", "");
        } else {
            log(rScore > cScore ? Affiliation.ROME : Affiliation.CARTHAGE,
                "WINNER", "Game won (scores (R,C): " + rScore + ", " + cScore + ")");
        }
        closeConnections();
    }

    public void run() throws InterruptedException {
        boolean doNext = false;

        do {
            Move move = moveQueue.poll(TIMEOUT_SEC, TimeUnit.SECONDS);

            /* Short cut game */
            if (System.currentTimeMillis() - start >= SHORT_CUT_MSEC) {
                System.out.println("Short cutting");
                log(program.getPlayer().getAffiliation(), "SHORT_CUT",move.toString());
                calculateScores();
                if (JudgeGetOpt.shortcut)
                    break;
            }

            if (move == null) {
                log(program.getPlayer().getAffiliation(), "PROTOCOL_ERROR", "<timeout>");
                if (!JudgeGetOpt.timeout)
                    continue;
                lost(program.getPlayer().getAffiliation(), "TIMEOUT");
                return;
            } else if (move instanceof JudgeWorker.InvalidMove) {
                JudgeWorker.InvalidMove iMove = (JudgeWorker.InvalidMove) move;
                log(iMove.player, iMove.comment, iMove.move);
                lost(iMove.player, iMove.comment);
                return;
            } else {
                doNext = move(move);
            }
        } while (doNext);

        closeConnections();
    }

    private boolean move(Move move) {
        System.out.println("> " + move);
        if (program.isOver())
            return false;

        if (move.player == Affiliation.ROME)
            carthage.sink.printf("%s\r\n", move);
        else
            rome.sink.printf("%s\r\n", move);

        result = program.move(move);

        if (gui != null)
            gui.update(program.graph);

        System.out.println(program.graph.toString() + " (" +
                           result.toDescriptiveString() + ")");

        log(move.player, result.toString(), move.toString());
        switch(result) {
        case INVALID_PLAYER:
            lost(program.getPlayer().affiliation, "Not active");
            return false;
        case INVALID_INPUT:
            lost(move.player, "Provided an invalid input");
            return false;
        case END_OF_GAME:
            calculateScores();
            return false;
        default:
            return true;
        }
    }

    /** Logs the map.
     *
     * This works around the fact that the map is read directly with a Scanner
     * and we cannot read the single lines by simply re-generating the map as
     * a string and then logging it.
     */
    private void logMap() {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        PrintStream ps = new PrintStream(baos);
        program.graph.printToStream(ps, "\n");
        for (String line : baos.toString().split("\n")) {
            log(Affiliation.ROME, "", line);
        }
    }

    public static void main(String[] args) throws Exception {
        JudgeGetOpt.getopt(args);

        Judge judge = new Judge();

        judge.log = new PrintStream(JudgeGetOpt.logFile);
        ServerSocket server = new ServerSocket(JudgeGetOpt.listenPort);
        Socket romeSocket = server.accept();

        judge.log(Affiliation.ROME, "ACCEPT", "Connection accepted");

        Socket carthageSocket;
        int connCount = 1;

        judge.start = System.currentTimeMillis();

        judge.log(Affiliation.CARTHAGE, "CONNECT", "Connecting to Carthage");
        for (; ; connCount++) {
            try {
                if (System.currentTimeMillis() - judge.start >= SHORT_CUT_MSEC)
                    throw new SocketTimeoutException("dummy timeout");
                carthageSocket = new Socket();
                carthageSocket.connect(new InetSocketAddress(JudgeGetOpt.remoteHost,
                                       JudgeGetOpt.remotePort), SHORT_CUT_MSEC);
                break;
            } catch (SocketTimeoutException e) {
                judge.log(Affiliation.CARTHAGE, "NOT_LISTENING",
                          "Not answering");
                judge.lost(Affiliation.CARTHAGE, "Timeout while connecting");
                return;
            } catch (java.net.ConnectException e) {
                if (connCount == 1)
                    judge.log(Affiliation.CARTHAGE, "NOT_LISTENING",
                              "Not listening yet");
                if (!JudgeGetOpt.retry) {
                    judge.lost(Affiliation.CARTHAGE, "Not listening");
                    return;
                }
                Thread.sleep(RETRY_WAIT_MSEC);
                continue;
            }
        }

        judge.log(Affiliation.CARTHAGE, "CONNECTED",
                  "Connected after " + connCount + " tries");

        /* Rome sends the map */
        InputStream stream = new SafeInputStream(romeSocket.getInputStream());
        Scanner scanner = new Scanner(stream);
        Program program;
        try {
            program = new Program(scanner, true);
        } catch (InputMismatchException e) {
            judge.log(Affiliation.ROME, "PROTOCOL_ERROR", "Invalid map!");
            judge.lost(Affiliation.ROME, "Invalid map");
            return;
        }

        if (JudgeGetOpt.gui) {
            judge.gui = new rac.gui.GUI(program.graph);
            judge.gui.show();
        }

        judge.program = program;
        judge.rome = new JudgeWorker(judge, Affiliation.ROME, romeSocket, scanner);
        judge.carthage = new JudgeWorker(judge, Affiliation.CARTHAGE, carthageSocket, null);

        /* Send carthage the map and start the game */
        judge.logMap();
        program.graph.printToStream(judge.carthage.sink, "\r\n");
        judge.rome.start();
        judge.carthage.start();
        /* Let's avoid the extra thread here */
        judge.run();
        judge.carthage.join();
        judge.rome.join();
        System.exit(0);
    }
}
