/*
 * Copyright 2014 Julian Andres Klode <klode@mathematik.uni-marburg.de>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package rac.graph;

import java.util.Iterator;
import java.util.Collection;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * A VertexSet contains vertices, de-duplicated.
 *
 * We use our own, tuned implementation here rather than Java's sets, because
 * our vertices use a consecutive range of integers starting at 0.
 */
public class VertexSet implements Iterable<Vertex> {
    /** Using an array here, because know the size in advance */
    protected Vertex[] vertices;

    /** Interface for a Visitor (see: Visitor Pattern) */
    public interface Visitor {
        boolean visit(Vertex individual);
    }

    private class VertexSetIterator implements Iterator<Vertex> {
        private int i = -1;

        @Override
        public boolean hasNext() {
            while (i + 1 < vertices.length && vertices[i + 1] == null) {
                i++;
            }
            return i + 1 < vertices.length && vertices[i + 1] != null;
        }

        @Override
        public Vertex next() {
            while (vertices[++i] == null)
                ;
            return vertices[i];
        }

        @Override
        public void remove() {
            VertexSet.this.remove(vertices[i]);
        }
    }

    /**
     * Construct a new Vertex set
     *
     * @param size The size of the set
     */
    public VertexSet(int size) {
        vertices = new Vertex[size];
    }

    /** Construct a new set from an iterable */
    public VertexSet(Iterable<Vertex> vertices) {
        int size = 0;
        for (Vertex vert : vertices)
            size = Math.max(vert.getId(), size);

        this.vertices = new Vertex[size + 1];

        for (Vertex vert : vertices)
            this.vertices[vert.getId()] = vert;
    }

    /** Return an iterator of vertices */
    @Override
    public java.util.Iterator<Vertex> iterator() {
        return new VertexSetIterator();
    }

    /** Check whether the vertex is contained in the set */
    public boolean contains(Vertex vertex) {
        return vertices[vertex.getId()] != null;
    }

    /**
     * Get a vertex based on its id
     *
     * @return The vertex, or null if it is not found.
     */
    public Vertex getById(int id) {
        return id < vertices.length ? vertices[id] : null;
    }

    /** Remove a vertex from the set */
    public void remove(Vertex vertex) {
        vertices[vertex.getId()] = null;
    }

    /** Remove all specified vertices from the set */
    public void removeAll(Iterable<Vertex> vertices) {
        for (Vertex v : vertices)
            remove(v);
    }

    /** Add a single vertex */
    public boolean add(Vertex v) {
        vertices[v.getId()] = v;
        return true;
    }

    /** Add multiple vertices */
    public void addAll(Iterable<Vertex> vertices) {
        for (Vertex v : vertices)
            add(v);
    }

    /**
     * Visit every vertex in the set.
     *
     * @return true if the visitor returned true for all vertices, false
     *         otherwise.
     */
    public boolean forAll(Visitor visitor)  {
        for (Vertex vertex : this)  {
            if (!visitor.visit(vertex))
                return false;
        }
        return true;
    }
    /**
     * Visit every vertex in the set.
     *
     * @return true if the visitor returned true for any vertices, false
     *         otherwise.
     */
    public boolean forAny(Visitor visitor)  {
        for (Vertex vertex : this)  {
            if (visitor.visit(vertex))
                return true;
        }
        return false;
    }

    /**
     * Calculate the size of the set.
     */
    public int size() {
        int i = 0;
        for (Vertex v : this)
            i++;
        return i;
    }

    /**
     * Check whether the set is empty
     */
    public boolean isEmpty() {
        for (int i = 0; i < vertices.length; i++)
            if (vertices[i] != null)
                return false;
        return true;
    }
}
