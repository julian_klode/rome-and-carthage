package rac.cli;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import rac.graph.*;

public class MoveProgram {
    public Graph graph1;
    public Graph graph2;
    public Move move;

    public MoveProgram(String path) throws FileNotFoundException {
        File file = new File(path);
        Scanner scanner = new Scanner(file);
        move = new Move(scanner.nextLine());
        graph1 = new Graph(scanner);
    }

    public void makeMove() {
        graph2 = graph1.makeMove(move);
    }

    public static void main(String[] args)  throws Exception {
        MoveProgram program = new MoveProgram(args[0]);
        System.out.printf("%s\n", program.graph1);
        System.out.printf("> %s\n", program.move);
        program.makeMove();
        System.out.printf("%s\n", program.graph2);
    }

}
