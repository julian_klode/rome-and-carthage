/*
 * Copyright 2014 Julian Andres Klode <klode@mathematik.uni-marburg.de>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package rac.cli.tests;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import static org.junit.Assert.*;
import org.junit.Test;

import rac.cli.Program;
import rac.graph.*;

public class GamePlayTest {

    @Test
    public void test41() throws FileNotFoundException {
        Program program = new Program("examples/4-1.txt");

        assertEquals(Program.Result.CONTINUE, program.move("R 2"));
        assertEquals("CNRR", program.graph.toString());
        assertEquals(Program.Result.CONTINUE, program.move("C 1"));
        assertEquals("CCNN", program.graph.toString());
        assertEquals(Program.Result.CONTINUE, program.move("R 2"));
        assertEquals("NNRN", program.graph.toString());
        assertEquals(Program.Result.CONTINUE, program.move("C 1"));
        assertEquals("NCRN", program.graph.toString());
        assertEquals(Program.Result.CONTINUE, program.move("R 3"));
        assertEquals("NCNN", program.graph.toString());
        /* Forfeit */
        assertEquals(Program.Result.FORFEIT, program.move("C X"));
        assertEquals("NCNN", program.graph.toString());
        assertEquals(Program.Result.END_OF_GAME, program.move("R X"));
        assertEquals("NCNN", program.graph.toString());
    }
    @Test
    public void test42() throws FileNotFoundException {
        Program program = new Program("examples/4-2.txt");

        assertEquals(Program.Result.CONTINUE, program.move("R 1"));
        assertEquals("NRNR", program.graph.toString());
        /* Move that has no effect */
        assertEquals(Program.Result.FORFEIT, program.move("C 0"));
        assertEquals("NRNR", program.graph.toString());
        assertEquals(Program.Result.END_OF_GAME, program.move("R X"));
        assertEquals("NRNR", program.graph.toString());
    }
    @Test
    public void test43() throws FileNotFoundException {
        Program program = new Program("examples/4-3.txt");

        assertEquals(Program.Result.CONTINUE, program.move("R 2"));
        assertEquals("CNRR", program.graph.toString());
        assertEquals(Program.Result.CONTINUE, program.move("C 1"));
        assertEquals("CCNN", program.graph.toString());
        /* Rome should be moving => Invalid */
        assertEquals(Program.Result.INVALID_PLAYER, program.move("C 0"));
        assertEquals("CCNN", program.graph.toString());
        /* Illegal move by chartage */
        assertEquals(Program.Result.END_OF_GAME, program.move("C 1"));
        assertEquals("CCNN", program.graph.toString());
    }
    @Test
    public void testRomeMovesFirst() throws FileNotFoundException {
        Program program = new Program("examples/4-3.txt");

        assertEquals(Program.Result.INVALID_PLAYER, program.move("C 0"));
        program = new Program("examples/4-3.txt");
        assertEquals(Program.Result.INVALID_PLAYER, program.move("C 1"));
        program = new Program("examples/4-3.txt");
        assertEquals(Program.Result.INVALID_PLAYER, program.move("C 2"));
        program = new Program("examples/4-3.txt");
        assertEquals(Program.Result.INVALID_PLAYER, program.move("C 3"));
    }

    @Test
    public void testIllegalMove() throws FileNotFoundException {
        Program program = new Program("examples/delta.txt");
        assertEquals(Program.Result.FORFEIT, program.move("R X"));
        assertEquals(Program.Result.CONTINUE, program.move("C 1"));
        assertEquals(Program.Result.FORFEIT, program.move("R X"));
        assertEquals(Program.Result.CONTINUE, program.move("C 2"));
        assertEquals(Program.Result.FORFEIT, program.move("R X"));
        /* Repeats initial board */
        assertEquals(Program.Result.END_OF_GAME, program.move("C 3"));
    }
}
