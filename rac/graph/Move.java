/*
 * Copyright 2014 Julian Andres Klode <klode@mathematik.uni-marburg.de>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package rac.graph;

/**
 * Implementation of a move
 */
public class Move {
    public Affiliation player;
    /** Either positive or -1 to forfeit */
    public int cityToAnnex;

    /** Forfeit for city */
    public static final int FORFEIT = -1;

    public Move(Affiliation player, int cityToAnnex) {
        this.player = player;
        this.cityToAnnex = cityToAnnex;
    }

    public Move(String moveString) {
        String[] elements = moveString.split(" ");

        player = Affiliation.fromString(elements[0]);
        if (player == Affiliation.NEUTRAL)
            throw new IllegalArgumentException("player must not be neutral");

        if (elements[1].equals("X")) {
            cityToAnnex = FORFEIT;
        } else {
            cityToAnnex = Integer.parseInt(elements[1]);
            if (cityToAnnex < 0)
                throw new IllegalArgumentException("city id must be positive");
        }
    }

    @Override
    public String toString() {
        return String.format("%s %s", player, cityToAnnex == FORFEIT ? "X" : Integer.toString(cityToAnnex));
    }
}
