/*
 * Copyright 2014 Julian Andres Klode <klode@mathematik.uni-marburg.de>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package rac.cli;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.HashSet;

import rac.graph.*;

public class ConsolePlayer extends Player {
    private InputStreamReader in = new InputStreamReader(System.in);
    private BufferedReader input = new BufferedReader(in);

    public ConsolePlayer(Program program, Affiliation affiliation) {
        super(program, affiliation);
    }

    @Override
    public Move calculateMove() {
        System.out.print("> ");
        try {
            return new Move(input.readLine());
        } catch (Exception e) {
            System.err.println("Invalid move");
            return new Move(affiliation.toString() + " X");
        }
    }

    /** Whether we ask on console */
    public boolean isInteractive() {
        return true;
    }
}
