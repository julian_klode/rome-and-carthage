/*
 * JudgeGetOpt - A simple option parser for the judge.
 *
 * Copyright 2014 Julian Andres Klode <klode@mathematik.uni-marburg.de>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package rac.cli;

import java.io.PrintStream;

public class JudgeGetOpt {
    public static boolean gui = false;
    public static int listenPort = 9999;
    public static String remoteHost = "localhost";
    public static int remotePort = 8888;
    public static String romeTag = "R";
    public static String carthageTag = "C";
    public static String logFile = "judge.log";
    public static boolean timeout = true;
    public static boolean shortcut = true;
    public static boolean retry = true;

    public static void help(PrintStream out) {
        String version = new JudgeGetOpt().getClass().getPackage().getImplementationVersion();
        if (version == null)
            version = "v1.0-unknown";
        out.printf("Rome and Carthage Judger %s\n", version);
        out.printf("Options:\n");
        out.printf("  -H, --host      Remote host name (default: %s)\n", remoteHost);
        out.printf("  -P, --port      Port to connect to (default: %d)\n", remotePort);
        out.printf("  -l, --listen    Port to listen to (default: %d)\n", listenPort);
        out.printf("  --tag-rome      The tag of rome (default: %s)\n", romeTag);
        out.printf("  --tag-carthage  The tag of rome (default: %s)\n", carthageTag);
        out.printf("  --log           Path to the log file (default: %s)\n", logFile);
        out.printf("  --no-timeout    Disable time outs\n", logFile);
        out.printf("  --no-shortcut   Disable short cutting\n");
        out.printf("  --no-retry      Do not try to connect to carthage multiple times\n");
        out.printf("  -g, --gui       Display game state in UI\n");
    }

    public static void getopt(String[] args) {
        for (int i = 0; i < args.length; i++) {
            switch(args[i]) {
            case "--host":
            case "-H":
                remoteHost = args[++i];
                break;
            case "--port":
            case "-P":
                remotePort = Integer.parseInt(args[++i]);
                break;
            case "--listen":
            case "-l":
                listenPort = Integer.parseInt(args[++i]);
                break;
            case "--log":
                logFile = args[++i];
                break;
            case "--help":
            case "-h":
                help(System.out);
                System.exit(0);
            case "--tag-rome":
                romeTag = args[++i];
                break;
            case "--tag-carthage":
                carthageTag = args[++i];
                break;
            case "-g":
            case "--gui":
                gui = true;
                break;
            case "--no-timeout":
                timeout = false;
                break;
            case "--no-shortcut":
                shortcut = false;
                break;
            case "--no-retry":
                retry = false;
                break;
            default:
                if (args[i].startsWith("-")) {
                    System.err.printf("Unknown option: %s\n", args[i]);
                    help(System.err);
                    System.exit(1);
                }
            }
        }
    }
}
