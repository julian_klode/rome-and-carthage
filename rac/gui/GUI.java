/*
 * Copyright 2014 Julian Andres Klode <klode@mathematik.uni-marburg.de>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package rac.gui;

import javax.swing.*;
import java.awt.*;

import rac.graph.*;


/**
 * Draw the simple GUI
 */
public class GUI extends JFrame {
    static final int BUTTON_WIDTH = 70;
    static final int BUTTON_HEIGHT = 50;
    private static final int PANEL_MARGIN = 50;

    private final JPanel panel;
    private final Graph graph;

    private final JButton[] buttons;

    public GUI (Graph g) {
        super();

        graph = g;
        panel = new GraphPanel(graph);
        panel.setLayout(null);
        buttons = new JButton[g.size()];

        setSize();
        addVertices();
        add(panel);
    }

    public void update(final Graph g) {
        SwingUtilities.invokeLater(new Runnable()  {
            public void run() {
                for (Vertex v: g) {
                    System.out.printf("Set %d to %s\n", v.getId(), v.toString());
                    buttons[v.getId()].setText(v.toString());
                }
            }
        });
    }

    /** Set the size of the window and the panel */
    private void setSize() {
        int minX = Integer.MAX_VALUE;
        int minY = Integer.MAX_VALUE;
        int maxX = 0;
        int maxY = 0;

        for (Vertex v : graph) {
            minX = Math.min(minX, v.getX());
            minY = Math.min(minY, v.getY());
            maxX = Math.max(maxX, v.getX());
            maxY = Math.max(maxY, v.getY());
        }

        /* Make sure everything is centered */
        int panelWidth = maxX  + BUTTON_WIDTH + minX;
        int panelHeight = maxY + BUTTON_HEIGHT + minY;

        panel.setSize(panelWidth, panelHeight);
        setMinimumSize(new Dimension(panelWidth + PANEL_MARGIN, panelHeight + PANEL_MARGIN));
    }

    /** Add the vertices to the window, as JButtons */
    private void addVertices() {
        for (Vertex v: graph) {
            JButton button = new JButton(v.toString());

            button.setBounds(v.getX(), v.getY(), BUTTON_WIDTH, BUTTON_HEIGHT);
            panel.add(button);
            buttons[v.getId()] = button;
        }
    }

    /** Start a new GUI. First argument must be the graph file */
    public static void main(final String[] args) throws Exception {
        java.io.File file = new java.io.File(args[0]);
        java.util.Scanner scanner = new java.util.Scanner(file);
        JFrame frame = new GUI(new Graph(scanner));
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }
}
