export CLASSPATH := /usr/share/java/junit4.jar:.

INPUT = $(shell find rac -name '*.java')
JAVA ?= java
JAVAC ?= javac

JAVA6 ?= /usr/lib/jvm/java-6-openjdk-amd64/bin/java
JAVAC6 ?= /usr/lib/jvm/java-6-openjdk-amd64/bin/javac

judge.jar all: $(INPUT)
	$(JAVAC) $(INPUT)
	( echo Main-Class: rac.cli.Judge; \
	  [ ! -e .git ] || echo Implementation-Version: $$(git describe); ) \
	  | jar cvfm judge.jar /dev/stdin rac/cli/*.class rac/graph/*.class rac/gui/*.class

judge.pub.jar: judge.jar
	proguard @judge.pro

test: judge.jar
	$(JAVA) -cp /usr/share/java/junit4.jar:. org.junit.runner.JUnitCore rac.graph.tests.Tests rac.cli.tests.Tests

cov: JAVA=$(JAVA6)
cov: JAVAC=$(JAVAC6)
cov:
	$(JAVAC) $(shell find rac -name '*.java' | grep -v Judge)
	$(JAVA) -jar /usr/share/java/emma.jar -sp $(CURDIR) -cp /usr/share/java/junit4.jar:/usr/share/java/cobertura.jar:. -r html  org.junit.runner.JUnitCore rac.graph.tests.Tests rac.cli.tests.Tests

tidy:
	astyle --style=java $(INPUT)



clean:
	find -name '*.class' -delete
	find -name '*.orig' -delete
	find -name '*.jar' -delete

doc:
	javadoc -private -d doc/ -sourcepath rac/ $(INPUT)

.PHONY: doc
