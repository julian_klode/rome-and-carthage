/*
 * Copyright 2014 Julian Andres Klode <klode@mathematik.uni-marburg.de>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package rac.graph;

import java.util.ArrayList;
import java.util.Collection;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Represent a vertex in the graph.
 */
final public class Vertex {
    private final int id;
    private final Affiliation affiliation;
    private final int x;
    private final int y;

    /**
     * Construct a new vertex with the affiliation changed.
     */
    public Vertex(final Vertex vertex, final Affiliation newAffiliation) {
        id = vertex.id;
        affiliation = newAffiliation;
        x = vertex.x;
        y = vertex.y;
    }

    /**
     * Construct a new Vertex from a Scanner.
     *
     * The first token shall be V. The second token shall be an integer ID. The
     * third token shall be an affiliation (see Affiliation.fromString()) and
     * tokens four and five shall be the x and y coordinates.
     */
    public Vertex(Scanner scanner) {
        String type = scanner.next(".");

        if (!type.equals("V")) {
            throw new InputMismatchException("Not a vertex: " + type);
        }

        id = scanner.nextInt();
        affiliation = Affiliation.fromString(scanner.next("."));
        x = scanner.nextInt();
        y = scanner.nextInt();
    }

    /** The ID of the vertex. */
    public int getId() {
        return id;
    }

    /** The affiliation */
    public Affiliation getAffiliation() {
        return affiliation;
    }

    /** Get the x-coordinate of the vertex */
    public int getX() {
        return x;
    }

    /** Get the y-coordinate of the vertex */
    public int getY() {
        return y;
    }

    /**  A short toString() that only contains ID and affiliation */
    @Override
    public String toString() {
        if (getAffiliation() == Affiliation.NEUTRAL) {
            return String.format("%d", getId());
        }

        return String.format("%d %s", getId(), getAffiliation());
    }
}
